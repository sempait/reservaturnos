﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.XtraScheduler;

public partial class Reportes : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //IMPORTANTE. Cuando se lee una variable de sesión 
        //hay que realizar una conversión de tipos (casting) al tipo del valor que almacena
        //en este caso es un boolean por esto se hace (bool) delante de la variable de sesion
        if ((Session["logado"] == null) || ((bool)Session["logado"] == false))
        {
            Response.Redirect("Login.aspx");


        }
        if (!IsPostBack)
        {
            TimeInterval interval = ASPxScheduler1.ActiveView.GetVisibleIntervals().Interval;
            ASPxDateEdit1.Date = interval.Start;
            ASPxDateEdit2.Date = interval.End;
        }
        PrepareReportPreview(PreviewPanel);

    }
    protected void ASPxCallbackPanel1_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        PreviewPanel.Visible = true;
    }
    void PrepareReportPreview(Control cp)
    {
        ASPxSchedulerControlPrintAdapter1.TimeInterval =
       new TimeInterval(DateTime.Today, DateTime.Today.AddDays(7));
        ASPxSchedulerControlPrintAdapter1.WorkTime =
            new TimeOfDayInterval(TimeSpan.FromHours(9), TimeSpan.FromHours(18));

        ReportPreview reportPreview = (ReportPreview)Page.LoadControl("ReportPreview.ascx");
        reportPreview.ControlAdapter = ASPxSchedulerControlPrintAdapter1;

        cp.Controls.Clear();
        cp.Controls.Add(reportPreview);

        //ASPxSchedulerControlPrintAdapter1.TimeInterval = GetPrintTimeInterval();

        //Reporting_ReportPreview reportPreview = (Reporting_ReportPreview)Page.LoadControl("ReportPreview.ascx");
        //reportPreview.ControlAdapter = ASPxSchedulerControlPrintAdapter1;
        //string reportName = ReportTemplateCombo.Text;
        //reportPreview.ReportTemplateFileName = "SchedulerReportTemplates\\" + reportName;


        //ASPxSchedulerControlPrintAdapter1.EnableSmartSync = reportName.ToLower().Contains("trifold");

        //cp.Controls.Clear();
        //cp.Controls.Add(reportPreview);
    }
    TimeInterval GetPrintTimeInterval()
    {
        DateTime start = ASPxDateEdit1.Date;
        DateTime end = ASPxDateEdit2.Date;
        return (start <= end) ? new TimeInterval(start, end) : new TimeInterval(end, start);
    }


    protected void lnkLogout_Click(object sender, EventArgs e)
    {
        Session.Clear();
        Session.Abandon();
        Response.Redirect("Login.aspx");
    }
}
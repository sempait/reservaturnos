﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default2.aspx.cs" Inherits="Default2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Reserva de Turnos Web</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">   
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">    
    <link href="css/style.css" rel="stylesheet">    
    <link href="css/pages/reports.css" rel="stylesheet">
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
  </head>
<body>
<form id="Form1" runat="server">
<div class="navbar navbar-fixed-top">	
	<div class="navbar-inner">
		<div class="container">
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			<a class="brand" href="Default.aspx">
				Reserva de Turnos Web				
			</a>		
			<div class="nav-collapse">
				<ul class="nav pull-right">
					<li class="dropdown">						
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon-cog"></i>
							Mi Cuenta
							<b class="caret"></b>
						</a>
						<ul class="dropdown-menu">
							<li><a href="Perfil.aspx">Configuracion</a></li>
							<li><a href="Default.aspx">Loguot</a></li>
                            
						</ul>						
					</li>								
				</ul>
			</div><!--/.nav-collapse -->	
		</div> <!-- /container -->
	</div> <!-- /navbar-inner -->
</div> <!-- /navbar -->
    
<div class="subnavbar">
	<div class="subnavbar-inner">
		<div class="container">
			<ul class="mainnav">
				<li class="active">
					<a href="Default.aspx">
						<i class="icon-list-alt"></i>
						<span>Inicio</span>
					</a>	    				
				</li>
                <li>					
					<a href="Caja.aspx">
						<i class="icon-bar-chart"></i>
						<span>Caja</span>
					</a>  									
				</li>
                <li>					
					<a href="Reportes.aspx">
						<i class="icon-print"></i>
						<span>Reportes</span>
					</a>  									
				</li>
				<li class="dropdown">					
					<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-long-arrow-down"></i>
						<span>Setup</span>
						<b class="caret"></b>
					</a>	
					<ul class="dropdown-menu">
                    	<li><a href="Usuarios.aspx">Usuarios</a></li>
						<li><a href="Clientes.aspx">Clientes</a></li>
                        <li><a href="Proveedores.aspx">Proveedores</a></li>
                        <li><a href="Recursos.aspx">Recursos</a></li>						
                    </ul>    				
				</li>
				<li>
					<a href="Default.aspx">
						<i class="icon-off"></i>
						<span>Logout</span>
					</a>    				
				</li>
			</ul>
		</div> <!-- /container -->
	</div> <!-- /subnavbar-inner -->
</div> <!-- /subnavbar -->
    
<div class="main">
	<div class="main-inner">
	    <div class="container">
	     <div class="row">
	      	<div class="span12">
                  <dx:ASPxScheduler ID="ASPxScheduler1" runat="server" 
                      AppointmentDataSourceID="SqlDataSourceAppointment" ClientIDMode="AutoID" 
                      ResourceDataSourceID="SqlDataSourceResources" Start="2014-03-15" GroupType="Resource" 
                      Theme="Metropolis">
                      <Storage>
                          <Appointments>
                              <Mappings AllDay="AllDay" AppointmentId="UniqueID" Description="Description" 
                                  End="EndDate" Label="Label" Location="Location" RecurrenceInfo="RecurrenceInfo" 
                                  ReminderInfo="ReminderInfo" ResourceId="ResourceID" Start="StartDate" 
                                  Status="Status" Subject="Subject" Type="Type" />
                          </Appointments>
                          <Resources>
                              <Mappings Caption="ResourceName" Color="Color" ResourceId="ResourceID" />
                          </Resources>
                      </Storage>
                        <Views>
                            <DayView ShowWorkTimeOnly="true" DayCount="2">
                                            <TimeRulers>
                            <dx:TimeRuler></dx:TimeRuler>
                            </TimeRulers>
                            <WorkTime Start="9:00" End="23:59" />
                            </DayView>
                            <WorkWeekView><TimeRulers>
                            <dx:TimeRuler></dx:TimeRuler>
                            </TimeRulers>
                            </WorkWeekView>
                            <WeekView Enabled="False">
                            </WeekView>
                            <MonthView CompressWeekend="False">
                            </MonthView>
                            <TimelineView Enabled="False">
                            </TimelineView>
                        </Views>
                  </dx:ASPxScheduler>

	      	      <asp:SqlDataSource ID="SqlDataSourceResources" runat="server" 
                      ConnectionString="<%$ ConnectionStrings:ReservaCanchaConnectionString %>" 
                      DeleteCommand="DELETE FROM [Resources] WHERE [UniqueID] = @UniqueID" 
                      InsertCommand="INSERT INTO [Resources] ([ResourceID], [ResourceName], [Color], [Image], [CustomField1]) VALUES (@ResourceID, @ResourceName, @Color, @Image, @CustomField1)" 
                      SelectCommand="SELECT * FROM [Resources]" 
                      UpdateCommand="UPDATE [Resources] SET [ResourceID] = @ResourceID, [ResourceName] = @ResourceName, [Color] = @Color, [Image] = @Image, [CustomField1] = @CustomField1 WHERE [UniqueID] = @UniqueID">
                      <DeleteParameters>
                          <asp:Parameter Name="UniqueID" Type="Int32" />
                      </DeleteParameters>
                      <InsertParameters>
                          <asp:Parameter Name="ResourceID" Type="Int32" />
                          <asp:Parameter Name="ResourceName" Type="String" />
                          <asp:Parameter Name="Color" Type="Int32" />
                          <asp:Parameter Name="Image" Type="String" />
                          <asp:Parameter Name="CustomField1" Type="String" />
                      </InsertParameters>
                      <UpdateParameters>
                          <asp:Parameter Name="ResourceID" Type="Int32" />
                          <asp:Parameter Name="ResourceName" Type="String" />
                          <asp:Parameter Name="Color" Type="Int32" />
                          <asp:Parameter Name="Image" Type="String" />
                          <asp:Parameter Name="CustomField1" Type="String" />
                          <asp:Parameter Name="UniqueID" Type="Int32" />
                      </UpdateParameters>
                  </asp:SqlDataSource>
                  <asp:SqlDataSource ID="SqlDataSourceAppointment" runat="server" 
                      ConflictDetection="CompareAllValues" 
                      ConnectionString="<%$ ConnectionStrings:ReservaCanchaConnectionString %>" 
                      DeleteCommand="DELETE FROM [Appointments] WHERE [UniqueID] = @original_UniqueID AND (([Type] = @original_Type) OR ([Type] IS NULL AND @original_Type IS NULL)) AND (([StartDate] = @original_StartDate) OR ([StartDate] IS NULL AND @original_StartDate IS NULL)) AND (([EndDate] = @original_EndDate) OR ([EndDate] IS NULL AND @original_EndDate IS NULL)) AND (([AllDay] = @original_AllDay) OR ([AllDay] IS NULL AND @original_AllDay IS NULL)) AND (([Subject] = @original_Subject) OR ([Subject] IS NULL AND @original_Subject IS NULL)) AND (([Location] = @original_Location) OR ([Location] IS NULL AND @original_Location IS NULL)) AND (([Description] = @original_Description) OR ([Description] IS NULL AND @original_Description IS NULL)) AND (([Status] = @original_Status) OR ([Status] IS NULL AND @original_Status IS NULL)) AND (([Label] = @original_Label) OR ([Label] IS NULL AND @original_Label IS NULL)) AND (([ResourceID] = @original_ResourceID) OR ([ResourceID] IS NULL AND @original_ResourceID IS NULL)) AND (([ResourceIDs] = @original_ResourceIDs) OR ([ResourceIDs] IS NULL AND @original_ResourceIDs IS NULL)) AND (([ReminderInfo] = @original_ReminderInfo) OR ([ReminderInfo] IS NULL AND @original_ReminderInfo IS NULL)) AND (([RecurrenceInfo] = @original_RecurrenceInfo) OR ([RecurrenceInfo] IS NULL AND @original_RecurrenceInfo IS NULL)) AND (([CustomField1] = @original_CustomField1) OR ([CustomField1] IS NULL AND @original_CustomField1 IS NULL))" 
                      InsertCommand="INSERT INTO [Appointments] ([Type], [StartDate], [EndDate], [AllDay], [Subject], [Location], [Description], [Status], [Label], [ResourceID], [ResourceIDs], [ReminderInfo], [RecurrenceInfo], [CustomField1]) VALUES (@Type, @StartDate, @EndDate, @AllDay, @Subject, @Location, @Description, @Status, @Label, @ResourceID, @ResourceIDs, @ReminderInfo, @RecurrenceInfo, @CustomField1)" 
                      OldValuesParameterFormatString="original_{0}" 
                      SelectCommand="SELECT * FROM [Appointments]" 
                      UpdateCommand="UPDATE [Appointments] SET [Type] = @Type, [StartDate] = @StartDate, [EndDate] = @EndDate, [AllDay] = @AllDay, [Subject] = @Subject, [Location] = @Location, [Description] = @Description, [Status] = @Status, [Label] = @Label, [ResourceID] = @ResourceID, [ResourceIDs] = @ResourceIDs, [ReminderInfo] = @ReminderInfo, [RecurrenceInfo] = @RecurrenceInfo, [CustomField1] = @CustomField1 WHERE [UniqueID] = @original_UniqueID AND (([Type] = @original_Type) OR ([Type] IS NULL AND @original_Type IS NULL)) AND (([StartDate] = @original_StartDate) OR ([StartDate] IS NULL AND @original_StartDate IS NULL)) AND (([EndDate] = @original_EndDate) OR ([EndDate] IS NULL AND @original_EndDate IS NULL)) AND (([AllDay] = @original_AllDay) OR ([AllDay] IS NULL AND @original_AllDay IS NULL)) AND (([Subject] = @original_Subject) OR ([Subject] IS NULL AND @original_Subject IS NULL)) AND (([Location] = @original_Location) OR ([Location] IS NULL AND @original_Location IS NULL)) AND (([Description] = @original_Description) OR ([Description] IS NULL AND @original_Description IS NULL)) AND (([Status] = @original_Status) OR ([Status] IS NULL AND @original_Status IS NULL)) AND (([Label] = @original_Label) OR ([Label] IS NULL AND @original_Label IS NULL)) AND (([ResourceID] = @original_ResourceID) OR ([ResourceID] IS NULL AND @original_ResourceID IS NULL)) AND (([ResourceIDs] = @original_ResourceIDs) OR ([ResourceIDs] IS NULL AND @original_ResourceIDs IS NULL)) AND (([ReminderInfo] = @original_ReminderInfo) OR ([ReminderInfo] IS NULL AND @original_ReminderInfo IS NULL)) AND (([RecurrenceInfo] = @original_RecurrenceInfo) OR ([RecurrenceInfo] IS NULL AND @original_RecurrenceInfo IS NULL)) AND (([CustomField1] = @original_CustomField1) OR ([CustomField1] IS NULL AND @original_CustomField1 IS NULL))">
                      <DeleteParameters>
                          <asp:Parameter Name="original_UniqueID" Type="Int32" />
                          <asp:Parameter Name="original_Type" Type="Int32" />
                          <asp:Parameter Name="original_StartDate" Type="DateTime" />
                          <asp:Parameter Name="original_EndDate" Type="DateTime" />
                          <asp:Parameter Name="original_AllDay" Type="Boolean" />
                          <asp:Parameter Name="original_Subject" Type="String" />
                          <asp:Parameter Name="original_Location" Type="String" />
                          <asp:Parameter Name="original_Description" Type="String" />
                          <asp:Parameter Name="original_Status" Type="Int32" />
                          <asp:Parameter Name="original_Label" Type="Int32" />
                          <asp:Parameter Name="original_ResourceID" Type="Int32" />
                          <asp:Parameter Name="original_ResourceIDs" Type="String" />
                          <asp:Parameter Name="original_ReminderInfo" Type="String" />
                          <asp:Parameter Name="original_RecurrenceInfo" Type="String" />
                          <asp:Parameter Name="original_CustomField1" Type="String" />
                      </DeleteParameters>
                      <InsertParameters>
                          <asp:Parameter Name="Type" Type="Int32" />
                          <asp:Parameter Name="StartDate" Type="DateTime" />
                          <asp:Parameter Name="EndDate" Type="DateTime" />
                          <asp:Parameter Name="AllDay" Type="Boolean" />
                          <asp:Parameter Name="Subject" Type="String" />
                          <asp:Parameter Name="Location" Type="String" />
                          <asp:Parameter Name="Description" Type="String" />
                          <asp:Parameter Name="Status" Type="Int32" />
                          <asp:Parameter Name="Label" Type="Int32" />
                          <asp:Parameter Name="ResourceID" Type="Int32" />
                          <asp:Parameter Name="ResourceIDs" Type="String" />
                          <asp:Parameter Name="ReminderInfo" Type="String" />
                          <asp:Parameter Name="RecurrenceInfo" Type="String" />
                          <asp:Parameter Name="CustomField1" Type="String" />
                      </InsertParameters>
                      <UpdateParameters>
                          <asp:Parameter Name="Type" Type="Int32" />
                          <asp:Parameter Name="StartDate" Type="DateTime" />
                          <asp:Parameter Name="EndDate" Type="DateTime" />
                          <asp:Parameter Name="AllDay" Type="Boolean" />
                          <asp:Parameter Name="Subject" Type="String" />
                          <asp:Parameter Name="Location" Type="String" />
                          <asp:Parameter Name="Description" Type="String" />
                          <asp:Parameter Name="Status" Type="Int32" />
                          <asp:Parameter Name="Label" Type="Int32" />
                          <asp:Parameter Name="ResourceID" Type="Int32" />
                          <asp:Parameter Name="ResourceIDs" Type="String" />
                          <asp:Parameter Name="ReminderInfo" Type="String" />
                          <asp:Parameter Name="RecurrenceInfo" Type="String" />
                          <asp:Parameter Name="CustomField1" Type="String" />
                          <asp:Parameter Name="original_UniqueID" Type="Int32" />
                          <asp:Parameter Name="original_Type" Type="Int32" />
                          <asp:Parameter Name="original_StartDate" Type="DateTime" />
                          <asp:Parameter Name="original_EndDate" Type="DateTime" />
                          <asp:Parameter Name="original_AllDay" Type="Boolean" />
                          <asp:Parameter Name="original_Subject" Type="String" />
                          <asp:Parameter Name="original_Location" Type="String" />
                          <asp:Parameter Name="original_Description" Type="String" />
                          <asp:Parameter Name="original_Status" Type="Int32" />
                          <asp:Parameter Name="original_Label" Type="Int32" />
                          <asp:Parameter Name="original_ResourceID" Type="Int32" />
                          <asp:Parameter Name="original_ResourceIDs" Type="String" />
                          <asp:Parameter Name="original_ReminderInfo" Type="String" />
                          <asp:Parameter Name="original_RecurrenceInfo" Type="String" />
                          <asp:Parameter Name="original_CustomField1" Type="String" />
                      </UpdateParameters>
                  </asp:SqlDataSource>

         </div>
         </div>   
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
<div class="footer">
	<div class="footer-inner">
		<div class="container">
			<div class="row">
    			<div class="span12">
    				&copy; 2014 <a href="http://www.sempait.com.ar">Reserva de Turnos Web</a>.
    			</div> <!-- /span12 -->
    		</div> <!-- /row -->
		</div> <!-- /container -->
	</div> <!-- /footer-inner -->
</div> <!-- /footer -->
    

<script src="js/jquery-1.7.2.min.js"></script>
<script src="js/excanvas.min.js"></script>
<script src="js/chart.min.js" type="text/javascript"></script>
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>

    var pieData = [
				{
				    value: 30,
				    color: "#F38630"
				},
				{
				    value: 50,
				    color: "#E0E4CC"
				},
				{
				    value: 100,
				    color: "#69D2E7"
				}

			];

    var myPie = new Chart(document.getElementById("pie-chart").getContext("2d")).Pie(pieData);

    var barChartData = {
        labels: ["January", "February", "March", "April", "May", "June", "July"],
        datasets: [
				{
				    fillColor: "rgba(220,220,220,0.5)",
				    strokeColor: "rgba(220,220,220,1)",
				    data: [65, 59, 90, 81, 56, 55, 40]
				},
				{
				    fillColor: "rgba(151,187,205,0.5)",
				    strokeColor: "rgba(151,187,205,1)",
				    data: [28, 48, 40, 19, 96, 27, 100]
				}
			]

    }

    var myLine = new Chart(document.getElementById("bar-chart").getContext("2d")).Bar(barChartData);
    var myLine = new Chart(document.getElementById("bar-chart1").getContext("2d")).Bar(barChartData);
    var myLine = new Chart(document.getElementById("bar-chart2").getContext("2d")).Bar(barChartData);
    var myLine = new Chart(document.getElementById("bar-chart3").getContext("2d")).Bar(barChartData);
	
	</script>

    </form>
  </body>

</html>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Clientes.aspx.cs" Inherits="Clientes" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Reserva de Turnos Web</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">   
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">    
    <link href="css/style.css" rel="stylesheet">    
    <link href="css/pages/reports.css" rel="stylesheet">
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
  </head>
<body>
<form id="Form1" runat="server">
<div class="navbar navbar-fixed-top">	
	<div class="navbar-inner">
		<div class="container">
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			<a class="brand" href="Default.aspx">
				Reserva de Turnos Web				
			</a>		
			<div class="nav-collapse">
				<ul class="nav pull-right">
					<li class="dropdown">						
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon-cog"></i>
							Mi Cuenta
							<b class="caret"></b>
						</a>
						<ul class="dropdown-menu">
							<li><a href="Perfil.aspx">Configuracion</a></li>
							<li>
                                <a>
                                    <asp:LinkButton ID="lnkLogout" runat=server Text="Logout" 
                                    onclick="lnkLogout_Click"></asp:LinkButton>
                                </a>
                            </li>
						</ul>						
					</li>								
				</ul>
			</div><!--/.nav-collapse -->	
		</div> <!-- /container -->
	</div> <!-- /navbar-inner -->
</div> <!-- /navbar -->
    
<div class="subnavbar">
	<div class="subnavbar-inner">
		<div class="container">
			<ul class="mainnav">
				<li>
					<a href="Default.aspx">
						<i class="icon-list-alt"></i>
						<span>Inicio</span>
					</a>	    				
				</li>
                <li>					
					<a href="Caja.aspx">
						<i class="icon-bar-chart"></i>
						<span>Caja</span>
					</a>  									
				</li>
                <li>					
					<a href="Reportes.aspx">
						<i class="icon-print"></i>
						<span>Reportes</span>
					</a>  									
				</li>
				<li class="active dropdown">					
					<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-long-arrow-down"></i>
						<span>Setup</span>
						<b class="caret"></b>
					</a>	
					<ul class="dropdown-menu">
                    	<li><a href="Usuarios.aspx">Usuarios</a></li>
						<li><a href="Clientes.aspx">Clientes</a></li>
                        <li><a href="Proveedores.aspx">Proveedores</a></li>
                        <li><a href="Recursos.aspx">Recursos</a></li>						
                    </ul>    				
				</li>
				
			</ul>
		</div> <!-- /container -->
	</div> <!-- /subnavbar-inner -->
</div> <!-- /subnavbar -->
    
<div class="main">
	<div class="main-inner">
	    <div class="container">
	     <div class="row">
	      	<div class="span12">
                  
                  <!-- /Recursos -->
                  <div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Clientes</h3>
	  				</div> <!-- /widget-header -->					
					<div class="widget-content">		
						
						<div class="tabbable">

                            <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" 
                                DataSourceID="SqlDataSourceClientes" EnableTheming="True" Width="100%" 
                                KeyFieldName="idCliente" Theme="Metropolis">
                                <Columns>
                                    <dx:GridViewCommandColumn VisibleIndex="0">
                                        <EditButton Visible="True">
                                        </EditButton>
                                        <NewButton Visible="True">
                                        </NewButton>
                                        <DeleteButton Visible="True">
                                        </DeleteButton>
                                    </dx:GridViewCommandColumn>
                                    <dx:GridViewDataTextColumn FieldName="idCliente" ReadOnly="True" 
                                        VisibleIndex="1">
                                        <EditFormSettings Visible="False" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="nombre" VisibleIndex="2">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="dni" VisibleIndex="3">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="telefono" VisibleIndex="4">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="foto" VisibleIndex="5">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="mail" VisibleIndex="6">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="ciudad" VisibleIndex="7">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="direccion" VisibleIndex="8">
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                            </dx:ASPxGridView>
						
						
                            
                            <asp:SqlDataSource ID="SqlDataSourceClientes" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:ReservaCanchaConnectionString %>" 
                                DeleteCommand="DELETE FROM [Cliente] WHERE [idCliente] = @idCliente" 
                                InsertCommand="INSERT INTO [Cliente] ([nombre], [dni], [telefono], [foto], [mail], [ciudad], [direccion]) VALUES (@nombre, @dni, @telefono, @foto, @mail, @ciudad, @direccion)" 
                                SelectCommand="SELECT * FROM [Cliente]" 
                                UpdateCommand="UPDATE [Cliente] SET [nombre] = @nombre, [dni] = @dni, [telefono] = @telefono, [foto] = @foto, [mail] = @mail, [ciudad] = @ciudad, [direccion] = @direccion WHERE [idCliente] = @idCliente">
                                <DeleteParameters>
                                    <asp:Parameter Name="idCliente" Type="Int32" />
                                </DeleteParameters>
                                <InsertParameters>
                                    <asp:Parameter Name="nombre" Type="String" />
                                    <asp:Parameter Name="dni" Type="String" />
                                    <asp:Parameter Name="telefono" Type="String" />
                                    <asp:Parameter Name="foto" Type="String" />
                                    <asp:Parameter Name="mail" Type="String" />
                                    <asp:Parameter Name="ciudad" Type="String" />
                                    <asp:Parameter Name="direccion" Type="String" />
                                </InsertParameters>
                                <UpdateParameters>
                                    <asp:Parameter Name="nombre" Type="String" />
                                    <asp:Parameter Name="dni" Type="String" />
                                    <asp:Parameter Name="telefono" Type="String" />
                                    <asp:Parameter Name="foto" Type="String" />
                                    <asp:Parameter Name="mail" Type="String" />
                                    <asp:Parameter Name="ciudad" Type="String" />
                                    <asp:Parameter Name="direccion" Type="String" />
                                    <asp:Parameter Name="idCliente" Type="Int32" />
                                </UpdateParameters>
                            </asp:SqlDataSource>
						
						
                            
                        </div>
                     </div>   
                  </div>
                  <!-- /End Recursos -->


            </div>
         </div>   
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
<div class="footer">
	<div class="footer-inner">
		<div class="container">
			<div class="row">
    			<div class="span12">
    				&copy; 2014 <a href="http://www.sempait.com.ar">Reserva de Turnos Web</a>.
    			</div> <!-- /span12 -->
    		</div> <!-- /row -->
		</div> <!-- /container -->
	</div> <!-- /footer-inner -->
</div> <!-- /footer -->
    

<script src="js/jquery-1.7.2.min.js"></script>
<script src="js/excanvas.min.js"></script>
<script src="js/chart.min.js" type="text/javascript"></script>
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>

    var pieData = [
				{
				    value: 30,
				    color: "#F38630"
				},
				{
				    value: 50,
				    color: "#E0E4CC"
				},
				{
				    value: 100,
				    color: "#69D2E7"
				}

			];

    var myPie = new Chart(document.getElementById("pie-chart").getContext("2d")).Pie(pieData);

    var barChartData = {
        labels: ["January", "February", "March", "April", "May", "June", "July"],
        datasets: [
				{
				    fillColor: "rgba(220,220,220,0.5)",
				    strokeColor: "rgba(220,220,220,1)",
				    data: [65, 59, 90, 81, 56, 55, 40]
				},
				{
				    fillColor: "rgba(151,187,205,0.5)",
				    strokeColor: "rgba(151,187,205,1)",
				    data: [28, 48, 40, 19, 96, 27, 100]
				}
			]

    }

    var myLine = new Chart(document.getElementById("bar-chart").getContext("2d")).Bar(barChartData);
    var myLine = new Chart(document.getElementById("bar-chart1").getContext("2d")).Bar(barChartData);
    var myLine = new Chart(document.getElementById("bar-chart2").getContext("2d")).Bar(barChartData);
    var myLine = new Chart(document.getElementById("bar-chart3").getContext("2d")).Bar(barChartData);
	
	</script>

    </form>
  </body>

</html>
